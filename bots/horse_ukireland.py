from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import psycopg2
import database
import time

DRIVER_PATH = "./chromedriver"

con = database.conn()
cur = con.cursor()
drop_table = "DROP TABLE IF EXISTS horse_uk_ireland"
create_table_query = '''CREATE TABLE IF NOT EXISTS horse_uk_ireland
          (ID SERIAL PRIMARY KEY     NOT NULL,
          regiao           TEXT    NOT NULL,
          cidade           TEXT    NOT NULL,
          hora_corrida           TEXT    NOT NULL,
          numero_corrida           TEXT    NOT NULL,
          competidor           TEXT    NOT NULL,
          odd           TEXT    NOT NULL,
          hora_leitura TIMESTAMP  DEFAULT CURRENT_TIMESTAMP); '''
try:    
  cur.execute(drop_table)
  con.commit()
  cur.execute(create_table_query)
  con.commit()
except (Exception, psycopg2.DatabaseError) as error :
    print ("Error while creatingtable", error)
finally:
  if(con):
    cur.close()
    con.close()

options = Options()
options.add_argument("start-maximized")
options.add_argument("disable-infobars")
options.add_argument("--disable-extensions")
#options.add_argument("--headless")

driver = webdriver.Chrome(options=options, executable_path=DRIVER_PATH)

driver.execute_script("Object.defineProperty(navigator, 'webdriver', {get: () => undefined})")

driver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
  "source": """
    Object.defineProperty(navigator, 'webdriver', {
      get: () => undefined
    })
  """
})

driver.get("https://www.bet365.com/#/AS/B2/")

#Pega as seções do país
WebDriverWait(driver, 1000).until(EC.presence_of_all_elements_located((By.CLASS_NAME, "gl-MarketGrid")))
market_grid = driver.find_elements_by_class_name("gl-MarketGrid")
market_group = market_grid[0].find_elements_by_class_name("rsm-MarketGroupWithTabs")
uk_ireland = market_group[1]
uk_ireland_country_name = uk_ireland.find_element_by_class_name("rsm-MarketGroupButton_Text").text
uk_ireland_cities = uk_ireland.find_elements_by_class_name("rsm-RacingSplashScroller")

foreach_city = len(uk_ireland_cities)
data = {}
k = 1
j = 0

for c in range(foreach_city):
  time.sleep(3)

  market_grid = WebDriverWait(driver, 1000).until(EC.presence_of_all_elements_located((By.CLASS_NAME, "gl-MarketGrid")))
  market_group = market_grid[0].find_elements_by_class_name("rsm-MarketGroupWithTabs")
  uk_ireland = market_group[1]

  cities_and_runnings = WebDriverWait(uk_ireland, 1000).until(EC.presence_of_all_elements_located((By.CLASS_NAME, 'rsm-RacingSplashScroller')))
  city_name = cities_and_runnings[c].find_element_by_class_name("rsm-MeetingHeader_MeetingName").text
  runnings = cities_and_runnings[c].find_elements_by_class_name("rsm-UKRacingSplashParticipant_Countdown")

  foreach_running = len(runnings)

  for r in range(foreach_running):
    runnings = WebDriverWait(driver, 1000).until(EC.presence_of_all_elements_located((By.CLASS_NAME, 'rsm-UKRacingSplashParticipant_Countdown')))
    driver.execute_script("arguments[0].click();", runnings[j])
    runnings = WebDriverWait(driver, 1000).until(EC.presence_of_all_elements_located((By.CLASS_NAME, 'srl-MarketEventHeaderInfo')))
    dogs_names = driver.find_elements_by_class_name("srm-ParticipantDetails_RunnerName")
    dogs_odds = driver.find_elements_by_class_name("srm-ParticipantHorseRacingOdds_Odds")
    grey_info = driver.find_element_by_class_name("srl-MarketEventHeaderInfo")
    greydata = grey_info.find_elements_by_class_name("srl-MarketEventHeaderInfo_Item")
    running_number =  driver.find_element_by_class_name("srl-ParticipantRacingNavTab-selected").text
    running_hour = greydata[1].text

    dogs = []
    odds = []
      
    for d in dogs_names:
      dogs.append(d.text)
    for o in dogs_odds:
      odds.append(o.text)
    
    insert_range = len(dogs)

    for n in range(insert_range):
      con = database.conn()
      cur = con.cursor()
      try:    
        insert_query = """ INSERT INTO horse_uk_ireland ( REGIAO, CIDADE, HORA_CORRIDA, NUMERO_CORRIDA, COMPETIDOR, ODD) VALUES (%s,%s,%s,%s,%s,%s)"""
        record = (uk_ireland_country_name, city_name, running_hour, running_number, dogs[n], odds[n])

        cur.execute(insert_query, record)
        con.commit()
        cur.close()
        con.close()
      except (Exception, psycopg2.DatabaseError) as error :
        print ("Error while creating table", error)

    j += 1
    k += 1
    
    time.sleep(3)
    driver.execute_script("window.history.go(-1)")

driver.quit()
  
  

#f = open("index.html", "a")
#f.write(str(str(driver.page_source).encode('ascii', 'ignore')))
#f.close()

