from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import psycopg2
import database
import time

DRIVER_PATH = "./chromedriver"

con = database.conn()
cur = con.cursor()
drop_table = "DROP TABLE galgos_virtual"
create_table_query = '''CREATE TABLE IF NOT EXISTS galgos_virtual
          (ID SERIAL PRIMARY KEY     NOT NULL,
          hora_corrida           TEXT    NOT NULL,
          competidor           TEXT    NOT NULL,
          odd           TEXT    NOT NULL,
          hora_leitura TIMESTAMP  DEFAULT CURRENT_TIMESTAMP); '''
try:    
  cur.execute(drop_table)
  con.commit()
  cur.execute(create_table_query)
  con.commit()
except (Exception, psycopg2.DatabaseError) as error :
    print ("Error while creatingtable", error)
finally:
  if(con):
    cur.close()
    con.close()

options = Options()
options.add_argument("start-maximized")
options.add_argument("disable-infobars")
options.add_argument("--disable-extensions")
options.add_argument('--no-sandbox')  

options.add_argument('--disable-dev-shm-usage')       
#options.add_argument("--headless")

driver = webdriver.Chrome(options=options, executable_path=DRIVER_PATH)

driver.execute_script("Object.defineProperty(navigator, 'webdriver', {get: () => undefined})")

driver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
  "source": """
    Object.defineProperty(navigator, 'webdriver', {
      get: () => undefined
    })
  """
})

driver.get("https://www.bet365.com")

#Pega as seções do país
WebDriverWait(driver, 1000).until(EC.presence_of_all_elements_located((By.CLASS_NAME, "gl-MarketGrid")))
market_grid = driver.find_elements_by_class_name("gl-MarketGrid")
market_group = market_grid[0].find_elements_by_class_name("rsm-MarketGroup")
virtual = market_group[1]
virtual_country_name = virtual.find_element_by_class_name("rsm-MarketGroupButton_Text").text
virtual_cities = virtual.find_elements_by_class_name("rsm-RacingSplashScroller")

foreach_city = len(virtual_cities)
data = {}
k = 1
j = 0

for c in range(foreach_city):
  time.sleep(3)

  WebDriverWait(driver, 1000).until(EC.presence_of_all_elements_located((By.CLASS_NAME, "gl-MarketGrid")))
  market_grid = driver.find_elements_by_class_name("gl-MarketGrid")
  market_group = market_grid[0].find_elements_by_class_name("rsm-MarketGroup")
  virtual = market_group[1]

  cities_and_runnings = WebDriverWait(virtual, 1000).until(EC.presence_of_all_elements_located((By.CLASS_NAME, 'rsm-RacingSplashScroller')))
  city_name = cities_and_runnings[c].find_element_by_class_name("rsm-MeetingHeader_MeetingName ").text
  runnings = cities_and_runnings[c].find_elements_by_class_name("rsm-RacingSplashParticipant ")

  foreach_running = len(runnings)

  for r in range(foreach_running):
    runnings = WebDriverWait(driver, 5).until(EC.presence_of_all_elements_located((By.CLASS_NAME, 'rsm-RacingSplashParticipant_RaceName ')))
    driver.execute_script("arguments[0].click();", runnings[j])
    WebDriverWait(driver, 5).until(EC.presence_of_all_elements_located((By.CLASS_NAME, 'gl-MarketGroup_Wrapper')))

    flag =  driver.find_elements_by_class_name('srg-VirtualRaceCardShowInfoBar_BookCloses')

    if len(flag) != 0:
      wrappers = driver.find_elements_by_class_name("srg-VirtualRacingParticipant_Wrapper")
      odd = driver.find_elements_by_class_name("srg-ParticipantGreyhoundsOdds_Odds")
      running_name = driver.find_element_by_class_name("srg-MarketEventHeaderMeeting_DropDown").text

      if len(wrappers) != 0 and len(odd) !=0:
        print("LEEEEEN", len(wrappers))
        name = driver.find_elements_by_class_name("srg-ParticipantDetails_RunnerName")
        odd = driver.find_elements_by_class_name("srg-ParticipantGreyhoundsOdds_Odds")

        dogs = []
        odds = []
    
        for n in name:
          dogs.append(n.text)
        for o in odd:
          odds.append(o.text)
        
        for_list = len(name)
        for n in range(for_list):
          con = database.conn()
          cur = con.cursor()
        
          try:    
            insert_query = """ INSERT INTO galgos_virtual (HORA_CORRIDA, COMPETIDOR, ODD) VALUES (%s,%s,%s)"""
            record = (running_name, dogs[n], odds[n])

            cur.execute(insert_query, record)
            con.commit()
            cur.close()
            con.close()
          except (Exception, psycopg2.DatabaseError) as error :
            print ("Error while creating table", error)

        j += 1
        driver.execute_script("window.history.go(-1)")
      else:
        driver.execute_script("window.history.go(-1)")
 
        j += 1
    else:
      j += 1
      driver.execute_script("window.history.go(-1)")
    
driver.quit()

#f = open("index.html", "a")
#f.write(str(str(driver.page_source).encode('ascii', 'ignore')))
#f.close()

